cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/cordova-plugin-vibration/src/browser/Vibration.js",
        "id": "cordova-plugin-vibration.Vibration",
        "pluginId": "cordova-plugin-vibration",
        "merges": [
            "navigator"
        ]
    },
    {
        "file": "plugins/cordova-plugin-vibration/www/vibration.js",
        "id": "cordova-plugin-vibration.notification",
        "pluginId": "cordova-plugin-vibration",
        "merges": [
            "navigator"
        ]
    },
    {
        "file": "plugins/cordova-sqlite-evcore-extbuild-free/www/SQLitePlugin.js",
        "id": "cordova-sqlite-evcore-extbuild-free.SQLitePlugin",
        "pluginId": "cordova-sqlite-evcore-extbuild-free",
        "clobbers": [
            "SQLitePlugin"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-vibration": "3.1.0",
    "cordova-sqlite-evcore-extbuild-free": "0.9.9-rc1",
    "cordova-plugin-whitelist": "1.3.3"
}
// BOTTOM OF METADATA
});